package com.liuxin.basicapplication;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    String input = "/storage/emulated/0/android/data/android/inputs";
    String output = "/storage/emulated/0/android/data/android/outputs";

    public static final String EXTRA_MESSAGE =
            "com.liuxin.basicapplication.extra.MESSAGE";

    public static void verifyStoragePermissions(Activity activity)
    {
        final int REQUEST_EXTERNAL_STORAGE = 1;
        final String[] PERMISSIONS_STORAGE = {
                "android.permission.INTERNET",
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE",
                "android.permission.MANAGE_EXTERNAL_STORAGE",
                "android.permission.ACCESS_MEDIA_LOCATION"
        };
        try
        {
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.WRITE_EXTERNAL_STORAGE");
            if (permission != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static List<String> getFilesAllName(String path)
    {
        File file = new File(path);
        File[] files = file.listFiles();
        List<String> imagePaths = new ArrayList<>();
        for(int i = 0; i < files.length; i++)
        {
            if(checkIsImageFile(files[i].getPath()))
            {
                String[] temp = files[i].getPath().split("/");
                imagePaths.add(temp[temp.length - 1]);
            }
        }
        return imagePaths;
    }

    public static boolean checkIsImageFile(String fName)
    {
        boolean isImageFile = false;
        String fileEnd = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if(fileEnd.equals("png"))
        {
            isImageFile = true;
        }
        else
        {
            isImageFile = false;
        }
        return isImageFile;
    }

    public Bitmap getBitmap(String file_path)
    {
        return BitmapFactory.decodeFile(file_path);
    }

    public void saveBitmap(Bitmap bitmap, File file_path) throws IOException
    {
        FileOutputStream out = new FileOutputStream(file_path);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        out.flush();
        out.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        verifyStoragePermissions(this);
    }

    public void doPhoto(View view) throws IOException
    {
        List<Bitmap> my_bitmaps = new ArrayList<>();
        List<String> my_files = getFilesAllName(input);
        // 遍历读取inputs文件夹，保存为bitmap格式
        for(int i = 0; i < my_files.size(); i++)
        {
            String input_file = input + "/" + my_files.get(i);
            my_bitmaps.add(getBitmap(input_file));
        }

        // TODO: 2022/5/12 在这里进行图片处理

        // 将经过处理的bitmap格式的图片，写入outputs文件夹
        for(int i = 0; i < my_files.size(); i++)
        {
            File output_file = new File(output + "/" + my_files.get(i));
            saveBitmap(my_bitmaps.get(i), output_file);
        }
    }
}